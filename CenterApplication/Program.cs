﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace CenterApplication
{
    class Program
    {
        static string[] WhosOnline = { "Oscar", "Kate", "Mum", "Dad", "Person1", "Person2", "Jesse", "Kita", "Jonty", "Jessie", "Sarah" };
        static TcpListener listener;
        const int LIMIT = 300; //5 concurrent clients

        static void Main(string[] args)
        {
            Console.WriteLine("***STARTING APPLICATION***");
            Console.WriteLine("");
            Console.WriteLine("Checking Internet");

            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                Console.WriteLine("Internet Working");
            }
            catch (Exception)
            {
                Console.WriteLine("***INTERNET DOWN. INTERNET DOWN. INTERNET DOWN**");
                Console.WriteLine("***CLOSING DOWN THE SERVER**");
                Console.ReadLine();
                return;
            }   

            Console.WriteLine("");
            Console.WriteLine("Opening Sockets");

            listener = new TcpListener(2055);
            listener.Start();
         
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Receive));
                t.Start();
            }


            Console.WriteLine("");
            Console.WriteLine("SERVER READY :-)");

            Console.WriteLine("");
            

            Console.ReadLine();
        }



        public static void Receive()
        {
           
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                Console.WriteLine(soc.RemoteEndPoint + " Polled the Server");
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true;

                    string message = "";
                    

                    for (int i = 0; i < WhosOnline.Length; i++)
                    {
                        message = message + WhosOnline[i];   
                    }

                    sw.WriteLine(message.ToString());





                    soc.Close();

                } catch
                {

                }
                

             
            }
        }

       






    }
}
