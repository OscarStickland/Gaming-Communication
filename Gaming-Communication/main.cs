﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Xml;

namespace Gaming_Communication
{
    public partial class Main : Form
    {
        TcpClient client;
        Stream s;
        StreamReader sr;
        StreamWriter sw;
        static TcpListener listener;
        bool IsPollServerRunning = true;
        
        public Main()
        {
            InitializeComponent();

        }

        

        public void PollServer()
        {
            while(IsPollServerRunning == true)
            {
                Connect();
                Thread.Sleep(5000);
                
            }
            
        }

        public void Connect()
        {
            
            try
            {
                client = new TcpClient("192.168.0.12", 2055);
                s = client.GetStream();
                sr = new StreamReader(s);
                sw = new StreamWriter(s);
                sw.AutoFlush = true;
                AllChatTextBox.Text = AllChatTextBox.Text + sr.ReadLine() + Environment.NewLine;
                

                listener = new TcpListener(2000);
                listener.Start();

                

                client.Close();
                s.Close();
                

                





            }
            catch
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                

            }
        }

        

        public void Disconnect()
        {
            if (client == null)
            {
                MessageBox.Show("Connection with Server Never Opened or has closed. If I am wrong, please close Gaming-Communication Through Task Manager.");
            } else
            {
                IsPollServerRunning = false;
                client.Close();
                s.Close();
                

            }
            
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(PollServer));
            t.Start();
        }

        private void DissconectButton_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Disconnect();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
    }
}
