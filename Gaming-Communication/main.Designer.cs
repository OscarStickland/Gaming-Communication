﻿namespace Gaming_Communication
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Oscar - 127.0.0.1");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Kate - 127.0.0.1");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("People", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.AllChatTextBox = new System.Windows.Forms.TextBox();
            this.TextToSayTextBox = new System.Windows.Forms.TextBox();
            this.SendButton = new System.Windows.Forms.Button();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.DissconectButton = new System.Windows.Forms.Button();
            this.PortTextBox = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // AllChatTextBox
            // 
            this.AllChatTextBox.Location = new System.Drawing.Point(113, 41);
            this.AllChatTextBox.Multiline = true;
            this.AllChatTextBox.Name = "AllChatTextBox";
            this.AllChatTextBox.Size = new System.Drawing.Size(523, 305);
            this.AllChatTextBox.TabIndex = 0;
            // 
            // TextToSayTextBox
            // 
            this.TextToSayTextBox.Location = new System.Drawing.Point(13, 355);
            this.TextToSayTextBox.Name = "TextToSayTextBox";
            this.TextToSayTextBox.Size = new System.Drawing.Size(548, 20);
            this.TextToSayTextBox.TabIndex = 1;
            // 
            // SendButton
            // 
            this.SendButton.Location = new System.Drawing.Point(568, 355);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(75, 19);
            this.SendButton.TabIndex = 2;
            this.SendButton.Text = "Send";
            this.SendButton.UseVisualStyleBackColor = true;
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(13, 12);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(309, 23);
            this.ConnectButton.TabIndex = 3;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // DissconectButton
            // 
            this.DissconectButton.Location = new System.Drawing.Point(492, 11);
            this.DissconectButton.Name = "DissconectButton";
            this.DissconectButton.Size = new System.Drawing.Size(144, 23);
            this.DissconectButton.TabIndex = 4;
            this.DissconectButton.Text = "Disconnect";
            this.DissconectButton.UseVisualStyleBackColor = true;
            this.DissconectButton.Click += new System.EventHandler(this.DissconectButton_Click);
            // 
            // PortTextBox
            // 
            this.PortTextBox.AllowDrop = true;
            this.PortTextBox.ImeMode = System.Windows.Forms.ImeMode.On;
            this.PortTextBox.Location = new System.Drawing.Point(329, 13);
            this.PortTextBox.Name = "PortTextBox";
            this.PortTextBox.Size = new System.Drawing.Size(100, 20);
            this.PortTextBox.TabIndex = 5;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(13, 41);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node1";
            treeNode1.Text = "Oscar - 127.0.0.1";
            treeNode2.Name = "Node3";
            treeNode2.Text = "Kate - 127.0.0.1";
            treeNode3.Name = "Node0";
            treeNode3.Text = "People";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3});
            this.treeView1.Size = new System.Drawing.Size(94, 305);
            this.treeView1.TabIndex = 6;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 387);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.PortTextBox);
            this.Controls.Add(this.DissconectButton);
            this.Controls.Add(this.ConnectButton);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.TextToSayTextBox);
            this.Controls.Add(this.AllChatTextBox);
            this.Name = "Main";
            this.Text = "Gaming - Communication";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox AllChatTextBox;
        private System.Windows.Forms.TextBox TextToSayTextBox;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Button DissconectButton;
        private System.Windows.Forms.TextBox PortTextBox;
        private System.Windows.Forms.TreeView treeView1;
    }
}

